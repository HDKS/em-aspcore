﻿using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {

            //サーバー接続
            string connstr = "userid=rezouser; password=Rezo_0000; database = rezodb; Host=127.0.0.1";
            MySqlConnection conn = new MySqlConnection(connstr);
            conn.Open();

            // データを格納するテーブル作成
            DataTable dt = new DataTable();

            // SQL文と接続情報を指定し、データアダプタを作成
            MySqlDataAdapter da = new MySqlDataAdapter("select * from employee", conn);

            // データ取得
            da.Fill(dt);

            Console.WriteLine(dt);

        }
    }
}
