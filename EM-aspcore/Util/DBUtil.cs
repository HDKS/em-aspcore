﻿using System;
using System.Data.SqlClient;

namespace EmAspCore.Util
{
    public class DBUtil
    {
        // 定数定義（共通） -> プロパティファイルに定義したい・・・
        private const string DB_HOST = "localhost";
	    private const string DB_NAME = "emdb";
	    private const string DB_USER = "emdb_user";
	    private const string DB_PASS = "12qwaszx";

        /// <summary>
        /// DB接続文字列
        /// </summary>
        public static string ConnectionString { get; set; }

        // インスタンス化禁止
        private DBUtil() { }

        // コネクションの単一化
        private static SqlConnection con;

        public static SqlConnection getConnection()
        {
            con = new SqlConnection();

            //string conStr = string.Format("Server={0}; Database={1}; User ID={2}; Password={3};"
            //                                                , DB_HOST, DB_NAME, DB_USER, DB_PASS);
            // 余談：最近の言語仕様なら↑のはこっちのがおススメ
            //string conStr = $"Server={DB_HOST}; Database={DB_NAME}; User ID={DB_USER}; Password={DB_PASS};";
            //con.ConnectionString = "Server=QS10R029OF007; Database=rezodb; Integrated Security=True;";
            //con.ConnectionString = conStr;

            if (string.IsNullOrEmpty(ConnectionString))
            {
                throw new Exception("DB設定ないです");
            }
            con.ConnectionString = ConnectionString;
            con.Open();

            return con;
        }
    }
}
