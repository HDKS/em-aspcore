﻿using EmAspCore.Dao;
using EmAspCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmAspCore.Services
{
    public class ResistService : BaseService
    {
        public ResistService() : base(true) { }

        public void ResistEmployee(Employee employee)
        {
            try
            {
                EmployeeDao dao = new EmployeeDao(base.con, base.tran);
                dao.InsertEmployee(employee);
                tran.Commit();

            }
            catch(Exception)
            {
                tran.Rollback();
            }
            finally
            {
                base.con.Close();
            }
            
        }
        public void UpdateEmployee(int id, Employee employee)
        {
            try
            {
                EmployeeDao dao = new EmployeeDao(base.con, base.tran);
                dao.UpdatetEmployee(id, employee);
                tran.Commit();
            }
            catch (Exception)
            {
                tran.Rollback();
            }
            finally
            {
                base.con.Close();
            }
        }
        public void DeleteEmployee(int id)
        {
            try
            {
                EmployeeDao dao = new EmployeeDao(base.con, base.tran);
                dao.DeleteEmployee(id);
                tran.Commit();
            }
            catch
            {
                tran.Rollback();
            }
            finally
            {
                base.con.Close();
            }
        }
    }
}
