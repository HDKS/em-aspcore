﻿using EmAspCore.Dao;
using EmAspCore.Entity;
using System.Collections.Generic;

namespace EmAspCore.Services
{
    public class SearchService : BaseService
    {
        // 検索系処理のため、AutoCommitして良い
        public SearchService() : base(false) { }

        public bool DoLogin(string mail, string pass)
        {
            // ToDo:ひとまずボルカニックバイパー
            return true;
        }

        /**
         * 社員検索を行う
         * Employeeに格納されている情報を検索キーとして利用
         * */
        public List<Employee> SelectAllEmployee()
        {
            try
            {
                // DAO呼び出し処理を書く
                // ホントはDIしたい・・・
                EmployeeDao dao = new EmployeeDao(base.con);
                return dao.SelectAll();
            }
            finally
            {
                base.con.Close();
            }
        }

        public List<Department> SelectAllDepartment()
        {
            try
            {
                DepartmentDao dao = new DepartmentDao(base.con);
                return dao.SelectAll();
            }
            finally
            {
                base.con.Close();
            }
        }
    }
}
