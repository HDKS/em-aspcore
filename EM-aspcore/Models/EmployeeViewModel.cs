﻿using EmAspCore.Entity;
using System.Collections.Generic;

namespace EmAspCore.Models
{
    public class EmployeeViewModel
    {
        // 検索結果の格納
        public List<Employee> employees { get; set; }
        public List<Department> departments { get; set; }

        public string empId { get; set; }
        public string empNm { get; set; }
        public string empKn { get; set; }
        public string mail { get; set; }
        public string newPass { get; set; }
        public string oldPass { get; set; }
        public string confirmPass { get; set; }
        public string department { get; set; }
    }
}
