﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace EmAspCore.Dao
{
    public abstract class BaseDao<T>
    {
        protected SqlConnection con;
        protected SqlTransaction tran;

        protected BaseDao(SqlConnection con)
        {
            this.con = con;
        }
        protected BaseDao(SqlConnection con, SqlTransaction tran)
        {
            this.con = con;
            this.tran = tran;
        }

        public List<T> SelectAll()
        {
            SqlCommand cmd = null;
            SqlDataAdapter adapter = null;
            List<T> retList = null;

            try
            {
                // SQLの組み立て
                var sql = new StringBuilder();
                sql.Append("SELECT * FROM " + this.getTableName());

                // SQL実行準備
                cmd = new SqlCommand(sql.ToString(), this.con);
                adapter = new SqlDataAdapter(cmd);

                // SQL実行
                var data = new DataTable();
                adapter.Fill(data);

                // DBの作りに依存しないようインスタンスに詰め替え
                retList = new List<T>();
                foreach (DataRow emp in data.Rows)
                {
                    retList.Add(this.RowMapping(emp));
                }
            } finally { 
                if (adapter != null) { adapter.Dispose(); }
                if (cmd != null) { cmd.Dispose(); }
            }

            // 検索結果をリターン
            return retList;
        }

        public abstract string getTableName();

        protected abstract T RowMapping(DataRow row);
 
    }
}
