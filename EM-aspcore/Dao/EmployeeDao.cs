﻿using EmAspCore.Entity;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace EmAspCore.Dao
{
    public class EmployeeDao : BaseDao<Employee>
    {
        public EmployeeDao(SqlConnection con) : base(con) { }
        public EmployeeDao(SqlConnection con, SqlTransaction tran) : base(con, tran) { }

        public List<Employee> SelectByKey(Employee key)
        {
            var sql = new StringBuilder();
            sql.Append("SELECT * FROM employee");

            // ToDo:ひとまずデバッグ用で全検索
            // ToDo:ここまでデバッグ


            // SQLの実行

            // DBの作りに依存しないようインスタンスに詰め替え
            List<Employee> retList = new List<Employee>();

            return retList;
        }

        public void InsertEmployee(Employee employee)
        {
            SqlCommand cmd = null;

            var sql = new StringBuilder();
            sql.Append("INSERT INTO " + this.getTableName());
            sql.Append("(");
            sql.Append("     nm_employee");
            sql.Append("    ,kn_employee");
            sql.Append("    ,mail_address");
            sql.Append("    ,password");
            sql.Append("    ,id_department");
            sql.Append(") VALUES (");
            sql.Append("     @nm");
            sql.Append("    ,@kn");
            sql.Append("    ,@mail");
            sql.Append("    ,@pass");
            sql.Append("    ,@dep");
            sql.Append(")");

            cmd = new SqlCommand(sql.ToString(), this.con);
            cmd.Transaction = tran;

            SqlParameter param = cmd.CreateParameter();
            param.ParameterName = "@nm";
            param.SqlDbType = SqlDbType.NChar;
            param.Direction = ParameterDirection.Input;
            param.Value = employee.employeeName;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@kn";
            param.SqlDbType = SqlDbType.NChar;
            param.Direction = ParameterDirection.Input;
            param.Value = employee.employeeKana;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@mail";
            param.SqlDbType = SqlDbType.NChar;
            param.Direction = ParameterDirection.Input;
            param.Value = employee.mailAddress;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@pass";
            param.SqlDbType = SqlDbType.NChar;
            param.Direction = ParameterDirection.Input;
            param.Value = employee.password;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@dep";
            param.SqlDbType = SqlDbType.Int;
            param.Direction = ParameterDirection.Input;
            param.Value = employee.departmentId;
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();
        }

        public void UpdatetEmployee(int id, Employee employee)
        {
            SqlCommand cmd = null;

            var sql = new StringBuilder();
            sql.Append(" UPDATE " + this.getTableName());
            sql.Append(" SET");
            sql.Append("     nm_employee = @nm");
            sql.Append("    ,kn_employee = @kn");
            sql.Append("    ,mail_address = @mail");
            sql.Append("    ,password = @pass");
            sql.Append("    ,id_department = @dep");
            sql.Append(" WHERE");
            sql.Append("     id_employee = @id");

            cmd = new SqlCommand(sql.ToString(), this.con);
            cmd.Transaction = tran;

            SqlParameter param = cmd.CreateParameter();
            param.ParameterName = "@nm";
            param.SqlDbType = SqlDbType.NChar;
            param.Direction = ParameterDirection.Input;
            param.Value = employee.employeeName;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@kn";
            param.SqlDbType = SqlDbType.NChar;
            param.Direction = ParameterDirection.Input;
            param.Value = employee.employeeKana;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@mail";
            param.SqlDbType = SqlDbType.NChar;
            param.Direction = ParameterDirection.Input;
            param.Value = employee.mailAddress;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@pass";
            param.SqlDbType = SqlDbType.NChar;
            param.Direction = ParameterDirection.Input;
            param.Value = employee.password;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@dep";
            param.SqlDbType = SqlDbType.Int;
            param.Direction = ParameterDirection.Input;
            param.Value = employee.departmentId;
            cmd.Parameters.Add(param);

            param = cmd.CreateParameter();
            param.ParameterName = "@id";
            param.SqlDbType = SqlDbType.Int;
            param.Direction = ParameterDirection.Input;
            param.Value = id;
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();
        }

        public void DeleteEmployee(int id)
        {
            SqlCommand cmd = null;

            var sql = new StringBuilder();
            sql.Append(" DELETE FROM " + this.getTableName());
            sql.Append(" WHERE id_employee = @id");
            cmd = new SqlCommand(sql.ToString(), this.con);
            cmd.Transaction = tran;

            SqlParameter param = cmd.CreateParameter();
            param.ParameterName = "@id";
            param.SqlDbType = SqlDbType.Int;
            param.Direction = ParameterDirection.Input;
            param.Value = id;
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();
        }

        public override string getTableName()
        {
            return "employee";
        }

        protected override Employee RowMapping(DataRow row)
        {
            Employee emp = new Employee();
            emp.employeeId = (int)row["id_employee"];
            emp.employeeName = (string)row["nm_employee"];
            emp.employeeKana = (string)row["kn_employee"];
            emp.mailAddress = (string)row["mail_address"];
            emp.password = (string)row["password"];
            emp.departmentId = (int)row["id_department"];
            return emp;
        }
    }
}
