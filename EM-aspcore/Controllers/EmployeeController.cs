﻿using EmAspCore.Entity;
using EmAspCore.Models;
using EmAspCore.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EmAspCore.Controllers
{
    public class EmployeeController : Controller
    {
        public IActionResult Index()
        {
            EmployeeViewModel model = new EmployeeViewModel();

            // 初期表示用に検索をしておく
            SearchService service = new SearchService();

            // Modelに詰める
            model.employees = service.SelectAllEmployee();
            model.departments = service.SelectAllDepartment();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                // collectionインスタンスから値を受け取りたい・・・
                Employee employee = new Employee();
                employee.employeeName = collection["empNm"];
                employee.employeeKana = collection["empKn"];
                employee.mailAddress = collection["mail"];
                employee.password = collection["newPass"];
                employee.departmentId = int.Parse(collection["department"]);

                // サービスモデルに処理を投げる
                ResistService service = new ResistService();
                service.ResistEmployee(employee);

            }
            catch
            {
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                // collectionインスタンスから値を受け取りたい・・・
                Employee employee = new Employee();
                employee.employeeId = int.Parse(collection["empId"]);
                employee.employeeName = collection["empNm"];
                employee.employeeKana = collection["empKn"];
                employee.mailAddress = collection["mail"];
                employee.password = collection["newPass"];
                employee.departmentId = int.Parse(collection["department"]);

                // サービスモデルに処理を投げる
                ResistService service = new ResistService();
                service.UpdateEmployee(employee.employeeId, employee);

            }
            catch
            {
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(IFormCollection collection)
        {
            try
            {
                int id = int.Parse(collection["empId"]);
                ResistService service = new ResistService();
                service.DeleteEmployee(id);
            }
            catch
            {
                
            }
            return RedirectToAction(nameof(Index));
        }
    }
}